<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class UserController extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    /**
     * Index login page
     */
    public function login(Request $request) {
        return view('front.login');
    }

    /**
     * Index sign in page
     */
    public function register(Request $request) {
        return view('front.register');
    }
}
