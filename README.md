
## Step1: Clone source code
- git clone ...

## Step2: Build docker image and service containers
- cd laravel-10-trainning/deployment
- docker-compose up -d

## Step3: Ssh into laravel_web container and run commands
- docker exec -it laravel_web bash **OR** winpty docker exec it laravel_web bash **(WINDOWS)**
- 「composer install」 or 「composer install --no-dev」
- php artisan key:generate
- php artisan storage:link
- chown root:apache -R .
- chmod 775 -R .
- mkdir logs

**Run more commands to migrate database and start apache**
- Run: php artisan migrate
- Run: echo 'IncludeOptional sites-enabled/*.conf' >> /etc/httpd/conf/httpd.conf
- Run: systemctl enable httpd
- Run: systemctl restart httpd.service

## Step5:  Run web
Frontend: http://localhost:8888/<br>
Database: http://localhost:9888/?server=laravel_db&username=root<br>
    password: root<br>
    database: mysql_dev<br>

**Next time run (docker-compose up -d) only to up those containers**<br>
